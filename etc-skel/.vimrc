:syn on
:set tabstop=3
:set expandtab
:set hlsearch
:set ls=2
:set statusline=%f%=[%{&ff}]\ [C%v,L%l/%L]
:set cm=blowfish2