FROM dx11-base

# GOLANG

ARG GO_VERSION=1.13.4
ENV GO_VERSION=$GO_VERSION

RUN curl -O https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz \
    && tar xf go${GO_VERSION}.linux-amd64.tar.gz \
    && rm go${GO_VERSION}.linux-amd64.tar.gz \
    && mv go /opt \
    && ln -s /opt/go/bin/go /usr/bin \
    && ln -s /opt/go/bin/gofmt /usr/bin
 
# NODEJS

ARG NODE_VERSION=v12.13.1
ENV NODE_VERSION=$NODE_VERSION

RUN curl -O https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-x64.tar.xz \
    && tar -xf node-${NODE_VERSION}-linux-x64.tar.xz -C /opt/ \
    && ln -s /opt/node-${NODE_VERSION}-linux-x64/bin/npm /usr/bin/ \
    && ln -s /opt/node-${NODE_VERSION}-linux-x64/bin/node /usr/bin/ \
    && ln -s /opt/node-${NODE_VERSION}-linux-x64/bin/npx /usr/bin/

# ANDROID SDK
# INSPIRED FROM https://www.coveros.com/running-android-tests-in-docker/

ARG ANDROID_SDK_VERSION=4333796
ENV ANDROID_SDK_VERSION=$ANDROID_SDK_VERSION
ARG ANDROID_PLATFORM="android-29"
ARG BUILD_TOOLS="29.0.0"
ENV ANDROID_PLATFORM=$ANDROID_PLATFORM
ENV BUILD_TOOLS=$BUILD_TOOLS

RUN apt install -y openjdk-8-jdk \
    && mkdir -p /opt/adk \
    && wget -q https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_VERSION}.zip \
    && unzip -q sdk-tools-linux-${ANDROID_SDK_VERSION}.zip -d /opt/adk \
    && rm sdk-tools-linux-${ANDROID_SDK_VERSION}.zip \
    && wget -q https://dl.google.com/android/repository/platform-tools-latest-linux.zip \
    && unzip -q platform-tools-latest-linux.zip -d /opt/adk \
    && rm platform-tools-latest-linux.zip \
    && yes | /opt/adk/tools/bin/sdkmanager --licenses \
    && /opt/adk/tools/bin/sdkmanager "build-tools;${BUILD_TOOLS}" "platforms;${ANDROID_PLATFORM}" "ndk-bundle" "lldb;3.1" "cmake;3.10.2.4988404" \
    && mkdir -p ${HOME}/.android/ \
    && ln -s /root/.android/avd ${HOME}/.android/avd \
    && ln -s /opt/adk/tools/emulator /usr/bin \
    && ln -s /opt/adk/platform-tools/adb /usr/bin \
    && chown -R 1000:1000 /opt/adk
ENV ANDROID_HOME /opt/adk

# FLUTTER

ARG FLUTTER_VERSION=v1.9.1+hotfix.6
ENV FLUTTER_VERSION=$FLUTTER_VERSION

RUN curl -O https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_${FLUTTER_VERSION}-stable.tar.xz \
    && tar -xf flutter_linux_${FLUTTER_VERSION}-stable.tar.xz -C /opt/ \
    && rm flutter_linux_${FLUTTER_VERSION}-stable.tar.xz \
    && ln -s /opt/flutter/bin/flutter /usr/bin/ \
    && chown -R 1000:1000 /opt/flutter

# ADD CUSTOM FILES AND SETTINGS

ADD ./container-files/start.sh /start.sh

# MAIN

CMD ["/start.sh"]
