FROM ubuntu:rolling
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y dbus-x11 procps psmisc && \
    apt-get install -y xdg-utils xdg-user-dirs menu-xdg mime-support desktop-file-utils && \
    apt-get install -y mesa-utils mesa-utils-extra libxv1

# Language/locale settings
#   replace en_US by your desired locale setting, 
#   for example de_DE for german.
ENV LANG en_US.UTF-8
RUN echo $LANG UTF-8 > /etc/locale.gen && \
    apt-get install -y locales && \
    update-locale --reset LANG=$LANG

# Mate desktop core
RUN apt-get install -y --no-install-recommends mate-desktop-environment-core \
	mate-applets appmenu-gtk2-module ca-certificates nautilus cryptsetup fonts-dejavu-core fonts-freefont-ttf \
	libmtp-runtime mate-icon-theme mate-indicator-applet mate-media mate-menus \
	mate-notification-daemon mate-polkit mate-settings-daemon terminator mate-utils \
	ubuntu-mate-core ubuntu-mate-default-settings xdg-user-dirs-gtk xkb-data \
	xorg zenity

# additional goodies
RUN apt-get install -y --no-install-recommends fonts-liberation gnome-keyring \
	gnome-menus indicator-application indicator-datetime indicator-messages indicator-notifications \
	indicator-power indicator-session laptop-detect mate-applet-appmenu mate-applet-brisk-menu \
	mate-calc mate-dock-applet mate-hud mate-menu mate-sensors-applet mate-system-monitor \
	mate-sntray-plugin mate-themes mate-tweak mate-window-buttons-applet mate-window-menu-applet \
	mate-window-title-applet network-manager network-manager-gnome network-manager-openvpn-gnome \
	network-manager-pptp-gnome plank policykit-desktop-privileges qt5-gtk-platformtheme qt5-style-platform-gtk2 \
	redshift-gtk smbclient tilda ubuntu-mate-artwork breeze-cursor-theme

# indicator-notifications qt5-style-platform-gtk2


# CORE COMPONENTS
RUN apt update && apt -y dist-upgrade && \
	apt install -y gnupg vim curl wget git build-essential fish net-tools dnsutils \
	netcat apt-utils iputils-ping iproute2 telnet zip unzip tmux && \
	apt -y autoremove --purge && apt -y clean

# ADDITIONAL APPS
RUN curl -s https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /root/microsoft.gpg && \
	install -o root -g root -m 644 /root/microsoft.gpg /etc/apt/trusted.gpg.d/ && rm /root/microsoft.gpg && \
	echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list && \
	apt update && apt install -y firefox code gnome-terminal file-roller htop keepass2 telegram-desktop gedit nitroshare && \
	curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb && \
	apt -y install ./keybase_amd64.deb && \
	rm keybase_amd64.deb && \
	apt -y autoremove --purge && apt -y clean

RUN apt install -y apt-transport-https curl && \
	curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add - && \
	echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | tee /etc/apt/sources.list.d/brave-browser-release.list && \
	apt update && apt install -y brave-browser

CMD ["mate-session"]
